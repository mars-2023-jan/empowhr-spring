package com.mars.login.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import org.springframework.beans.factory.annotation.Value;

@Configuration
public class DynamoDBConfig {
	
	@Value("${aws.dynamodb.endpoint}")
    private String awsDynamoDBEndpoint;

    @Value("${aws.dynamodb.region}")
    private String awsDynamoDBRegion;

    @Value("${aws.dynamodb.accesskey}")
    private String accessKey;

    @Value("${aws.dynamodb.secretkey}")
    private String secretKey;
    
	@Bean
	 public DynamoDBMapper mapper(){
	   return new DynamoDBMapper(amazonDynamoDBConfig());
	 }
	 private AmazonDynamoDB amazonDynamoDBConfig(){
		 System.out.println("Inside Dynamo DB Config");
	   return AmazonDynamoDBClientBuilder.standard()
		.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(awsDynamoDBEndpoint,awsDynamoDBRegion))
		.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey,secretKey))).build();	
	 }

}
