package com.mars.login.repository;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBSaveExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;
import com.amazonaws.services.dynamodbv2.model.Condition;

import com.mars.login.entity.Task;
import com.mars.login.entity.User;

@Repository
public class TaskRepository {
	@Autowired
	private DynamoDBMapper mapper;
	
	public Task addTask (Task task) {
		String id = UUID.randomUUID().toString();
        task.setTask_id(id);
		mapper.save(task);
		return task;
	}
	
	public Task editTask(Task task) {
		mapper.save(task);
		return task;
	}
	
	public void setTaskToComplete(String task_id) {
	    // Get the task from DynamoDB.
	    TaskRepository taskRepository = new TaskRepository();
	    Task task = taskRepository.findTaskById(task_id);

	    // Update the date_complete of the task.
	    long timestamp = System.currentTimeMillis();
	    task.setDate_complete(timestamp);

	    // Save the task to DynamoDB.
	    mapper.save(task);
	}

	public Task findTaskById(String task_id) {
		return mapper.load(Task.class, task_id);
	}
	
	public List<Task> findAllTasksByEmail(String email) {
		// Get the tasks from DynamoDB.
		return mapper.scan(Task.class, new DynamoDBScanExpression());

	}
	//

}
