package com.mars.login.repository;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBSaveExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;
import com.mars.login.entity.User;

@Repository
public class UserRepository {
	@Autowired
	private DynamoDBMapper mapper;

	public User addUser(User user) {
		mapper.save(user);
		return user;
	}

	public User findUserByEmail(String email) {
		return mapper.load(User.class, email);
	}

	public String deleteUser(User user) {
		mapper.delete(user);
		return "User Deleted";
	}

	public String editUser(User user) {
		mapper.save(user);
		return "User Updated";
	}

	public List<User> getAllUsers() {
		// generate code to get all users and return them
		return mapper.scan(User.class, new DynamoDBScanExpression());
	}

	public String authenticateUser(User user) {

		// mapper.load(User.class, user.getEmail());
		System.out.println("authenticateUser()");
		User user2 = findUserByEmail(user.getEmail());
		if (user2 == null) {
			return null;
		}
		
		if (!user.getPassword().equals(user2.getPassword())) {
			return null;
		}
			
		System.out.println("Auth Success");
		return user2.getEmail();

	}

	private DynamoDBSaveExpression buildExpression(User user) {
		DynamoDBSaveExpression dynamoDBSaveExpression = new DynamoDBSaveExpression();
		HashMap<String, ExpectedAttributeValue> expectedMap = new HashMap<>();
		expectedMap.put("email", new ExpectedAttributeValue(new AttributeValue().withS(user.getEmail())));
		dynamoDBSaveExpression.setExpected(expectedMap);
		return dynamoDBSaveExpression;
	}

}
