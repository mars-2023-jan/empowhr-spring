package com.mars.login.controller;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mars.login.entity.LoginInfo;
import com.mars.login.entity.User;
import com.mars.login.repository.UserRepository;

@RestController
public class UserController {
	@Autowired
	public UserRepository userRepo;

	@PostMapping("/api/user/create")
	public ResponseEntity<String> saveUser(@RequestBody User user) {
		userRepo.addUser(user);
		return new ResponseEntity<>( "UserAdded", HttpStatus.OK);
	}

	@GetMapping("/api/user/{email}")
	public ResponseEntity<User> findUser(@PathVariable String email) {
		User finduser = userRepo.findUserByEmail(email);
		if (finduser != null) {
			return new ResponseEntity<>( finduser, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/api/users")
	public ResponseEntity<List<User>>  getAllUsers() {
		return new ResponseEntity<List<User>>(userRepo.getAllUsers(), HttpStatus.OK);
	}

	@DeleteMapping("/api/user/delete")
	public ResponseEntity<String>  deleteUser(@RequestBody User user) {
		userRepo.deleteUser(user);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PutMapping("/api/user/update")
	public ResponseEntity<String> updateUser(@RequestBody User user) {
		userRepo.editUser(user);
		return new ResponseEntity<>(HttpStatus.OK);
	}


	@PostMapping("/api/user/login")
	public ResponseEntity<String> authenticateUser(@RequestBody User user ) {
		System.out.println("LOGIN");
		String response = userRepo.authenticateUser(user);
		if (response!=null) {
			return new ResponseEntity<String>(response, HttpStatus.OK);
		} else { 
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}

}


