package com.mars.login.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mars.login.entity.Task;
import com.mars.login.entity.User;
import com.mars.login.repository.TaskRepository;

@RestController
public class TaskController {
	@Autowired
	public TaskRepository taskRepository;
	
	@PostMapping("/api/task/create")
	public ResponseEntity<Task> addTask(@RequestBody Task task) {
		System.out.println(task);
		taskRepository.addTask(task);
		
		return new ResponseEntity<Task>(task, HttpStatus.CREATED);
	}
	@GetMapping("/api/tasks/{email}")
	public ResponseEntity<List<Task>>  findAllTasksByEmail(@PathVariable String email) {
		return new ResponseEntity<List<Task>>(taskRepository.findAllTasksByEmail(email), HttpStatus.OK);
	}
	@GetMapping("/api/task/{task_id}")
	public ResponseEntity<Task>  findTaskById(@PathVariable String task_id) {
		Task task = taskRepository.findTaskById(task_id);
		return new ResponseEntity<Task>(task, HttpStatus.OK);
	}
	@PutMapping("/api/task/complete/{task_id}")
	public ResponseEntity<Task> setTaskToComplete(@PathVariable String task_id) {
	    // Get the task from DynamoDB.
	    Task task = taskRepository.findTaskById(task_id);

	    // Update the date_complete of the task.
	    task.setDate_complete(System.currentTimeMillis());

	    // Save the task to DynamoDB.
	    taskRepository.editTask(task);

	    return new ResponseEntity<>(task, HttpStatus.OK);
	}
}
