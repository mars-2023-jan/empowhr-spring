package com.mars.login.entity;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Data;

@Data
@DynamoDBTable(tableName = "users")                    
public class User implements Serializable{
  @DynamoDBHashKey(attributeName = "email")	
  
  @DynamoDBAttribute
  private String email;

  @DynamoDBAttribute
  private String password;

  @DynamoDBAttribute
  private String name;

}

