package com.mars.login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmpowhrSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmpowhrSpringApplication.class, args);
	}

}
